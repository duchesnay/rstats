# 1. Dataset: Student score with parent education level with class random effect

## Questions

Recode in R the models used in the course:

1. Set working directory `setwd()` to your settings.
2. Read `score_parentedu_byclass.csv` csv file.
3. Global fixed effect model.
4. Write a function `rmse_coef_tstat_pval <- function(mod, var)` that returns:
    - the global RMSE
    - the coefficient (associated with the independant variable `var`)
    - the t-value (associated with the independant variable `var`)
    - the p-value (associated with the independant variable `var`) of a fitted model `mod`.
5. Model a classroom intercept as a fixed effect: ANCOVA.
6. Model using aggregation of data into independent units average over classroom aggregates.
7. Model using Hierarchical/multilevel modeling.
    - Suggestion use `lmList()` function.
    - Install/load `lme4` library.
8. Model the classroom random intercept: linear mixed model.
    - install/load `lmerTest` library to get p-value.


# 2. Ratpup dataset: Two-Level Models for Clustered Data

## Introduction

Ratpup dataset: https://rdrr.io/rforge/WWGbook/man/ratpup.html
Quote from [Brady et al. 2014]: The Rat Pup data is an example of a two-level
clustered data set obtained from a cluster-randomized trial: each litter (cluster) was randomly assigned to a specific level of treatment,
and rat pups (units of analysis) were nested within litters. The birth weights of rat
pups within the same litter are likely to be correlated because the pups shared the same
maternal environment. In models for the Rat Pup data, we include random litter effects
(which imply that observations on the same litter are correlated) and fixed effects associated with treatment. Our analysis uses a two-level random intercept model to compare
the mean birth weights of rat pups from litters assigned to the three different doses, after
taking into account variation both between litters and between pups within the same
litter. Ratpup is a two-level (rat level 1, and litter level 2) random effect model.

Variables:
    - pup.id: Unique identifier for each rat pup
    - weight: Birth weight of the rat pup (the dependent variable)
    - sex: Sex of the rat pup (Male, Female)
    - litter: Litter ID number
    - litsize: Litter size (i.e., number of pups per litter)
    - treatment: Dose level of the experimental compound assigned to the litter (High, Low, Control)

**Objective: Explore sex effect on weight**

## Questions

1. Model global sex effect (biased)
    - Residuals diagnosis
2. Model a litter intercept as a fixed effect: ANCOVA (biased)
    - Explore model internals
    - Residuals diagnosis
3. Hierarchical model
4. Model the litter random intercept: linear mixed model


# 3. Sleepstudy dataset: Longitudinal study

## Introduction

The average reaction time per day for subjects in a sleep deprivation study.
On day 0 the subjects had their normal amount of sleep.
Starting that night they were restricted to 3 hours of sleep per night.
The observations represent the average reaction time on a series of tests given each day to each subject.
A data frame with 180 observations on the following 3 variables.

Variables:

- Reaction: Average reaction time (ms)
- Days: Number of days of sleep deprivation
- Subject: Subject number on which the observation was made.

This is a two level model:
    - Level 1 (within subject at time level): _Days_ (fixed effect independant var) and _Reaction_ (dependant var.)
    - Level 2 (between subjects at subject level): _Subject_ (mixed effect)

**Objective: model the effects of time (_Days_) on _Reaction_ time.**

## Questions

1. Model Global Days effect (biased)
2. Model a Subject intercept as a fixed effect: ANCOVA (biased)
3. Hierarchical/multilevel modeling
4. Model the subject random intercept: linear mixed model
5. Model the subject intercept and slope as a fixed effect: ANCOVA with interactions
6. Model the subject random intercept and slope with LMM

