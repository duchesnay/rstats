# Statistics in R
---

## Ressources

- [Repository URL](https://bitbucket.org/duchesnay/rstats/)
- [Download URL](https://bitbucket.org/duchesnay/rstats/downloads/)

## STAT1: Introduction à l'analyse de données

Objectif : Se familiariser avec le parcours de formation et s’orienter dans le choix de modules adaptés aux domaines d’activités et aux besoins statistiques 

1. L'analyse de données : une partie émergée de l'iceberg

	- La science des données "data science"
	- Les outils informatiques

2. Objectif: autonomie en statistique

	- le design d'étude (taille de l'échantillon, etc.)
	- la manipulation de données
	- les analyses simples: quel test statistique ?

3. Objectif: autonomie en outils informatique

	- Manipuler les données
	- Pourquoi l'open source ?
	- L'écosystème R open-sources pour l'analyse de données
	- Apprendre à programmer ? Oui et non
	- Approche graphique vs programme
	- Un programme est une recette de cuisine
	- Les différents niveaux d'autonomie
	- Appliquer une recette
	- Concevoir une nouvelle recette

## STAT2: De la description à l’inférence statistique

Objectif : Comprendre les statistiques descriptives, l’analyse exploratoire graphique des données et les bases de l’inférence en statistique ; savoir caractériser la distribution d’une variable, construire un intervalle de confiance pour une moyenne ou une proportion et réaliser des tests simples sur un ou deux échantillons 

- Rappels sur les types de variables et les principaux indicateurs statistiques (moyenne, médiane, écart-type, quartiles, etc.)
- Analyse exploratoire des données et techniques de visualisation
- Principes de base de l’analyse inférentielle: **test d'hypothèse**
- Estimation d’un paramètre et construction d’un intervalle de confiance
- Tests statistiques pour la comparaison de deux moyennes et de deux proportions

## STAT3: Choisir un test statistique

- Différencier les statistiques descriptives des statistiques inférentielles.
- Comprendre les notions de probabilité conditionnelle et d’indépendance stochastique
- Savoir réaliser un plan d’analyses statistiques
- Utiliser le test statistiques le plus pertinent par rapport à leur besoin (notion de puissance, d’intervalle de confiance, homogénéité/adéquation)
- Savoir utiliser et interpréter les principaux tests (paramétriques ou non paramétriques)

	* Fisher / Anova (modèle fixe)
	* Test de comparaisons multiples de moyenne
	* Principaux tests de comparaison de la moyenne
	* Mann-Whitney ....
	* Analyse de la variance à un facteur
	* Analyse de la variance à plusieurs facteurs
	* Plans complets et interaction
	* Analyse de la covariance

## STAT21: Modèles lineaires mixtes

- Observations structurées: répétées, longitudinales ou groupées en cluster
- Modèles à effets fixes
- Modèles à effets aléatoires
- Limites et problèmes des modèles à effets fixes
- Modèles hierarchiques
- Modèle à effet aléatoire (1): random intercept
- Modèle à effet aléatoire (2): random slope
- Theorie


