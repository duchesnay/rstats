---
title: "Testing Pairwise Associations"
author: "Edouard Duchesnay (edouard.duchesnay@gmail.com)"
date: "June 5, 2018"
output:
  pdf_document: default
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Resources:

- Git repository: [bitbucket.org/duchesnay/rstats](https://bitbucket.org/duchesnay/rstats)
- Download archive: [bitbucket.org/duchesnay/rstats/downloads](https://bitbucket.org/duchesnay/rstats/downloads/)

![Choice of a statistical test](figs/stat_tests_flowchart.png)

Set working directory

```{r}
setwd("/home/ed203246/git/rstats/")
```

## Variable type

Sources:

- <http://www.r-tutor.com/elementary-statistics> (in french)

- <http://www.ilovestatistics.be/concepts/concepts-base.html>

Mass univariate statistical analysis: explore association betweens pairs of variable. 

- In statistics, a **categorical variable** or **factor** is a variable that can take on one of a limited, and usually fixed, number of possible values, thus assigning each individual to a particular group or "category". **The levels** are the possibles values of the variable. Number of levels = 2: binomial; Number of levels > 2: multinomial. There is no intrinsic ordering to the categories.  For example, gender is a categorical variable having two categories (male and female) and there is no intrinsic ordering to the categories. For example, Sex (Female, Male), Hair color (blonde, brown, etc.).

- An **ordinal variable** is a categorical variable with a clear ordering of the levels. For example: drinks per day (none, small, medium and high).

- A **continuous** or **quantitative variable** $x \in \mathbb{R}$ is one that can take any value in a range of possible values, possibly infinite.  E.g.: salary, experience in years, weight.

What statistical test should I use? See: http://www.ats.ucla.edu/stat/mult_pkg/whatstat/

## Modeling data

Study association between age and sbp (systolic blood pressure).

1. model the data sbp ~ age:
\begin{align}
sbp &= \beta_1 age + baseline + \varepsilon\\
sbp &= \beta_1 age + \beta_0 + \varepsilon
\end{align}

2. fit the model: Estimate the model parameters: $\beta_1, \beta_0$ using mean squared error.
3. test

## Variable role in a model

Model in R syntax: **Indendant variable ~ dependant variable(s)**

- Indendant variable(s) (IV, age): predictors, "variable explicative", regressor(s)
- dependant variable (DV, sbp): target variable, "variable à expliquer"
- Here: sbp ~ age

## Pearson correlation test: test association between two quantitative variables

Test the correlation coefficient of two quantitative variables. The test calculates a Pearson correlation coefficient and the $p$-value for testing non-correlation.

Let $x$ and $y$ two quantitative variables, where $n$ samples were obeserved. The linear correlation coefficient is defined as :

$$r=\frac{\sum_{i=1}^n(x_i-\bar x)(y_i-\bar y)}{\sqrt{\sum_{i=1}^n(x_i-\bar x)^2}\sqrt{\sum_{i=1}^n(y_i-\bar y)^2}}.$$

Under $H_0$, the test statistic $t=\sqrt{n-2}\frac{r}{\sqrt{1-r^2}}$ follow Student distribution with $n-2$ degrees of freedom.

### Exercise: simulated data

Generate 50 samples of age uniformly distributed in range of $[20, 40]$.
Create $y = 2 * age + \varepsilon$, where the noise, $\varepsilon \sim \mathcal{N}(\mu=0, \sigma=1)$. 

1. Test the association between $age$ and $y$.

### Solution

```{r}
n = 100
age = runif(n, min = 20, max = 40)
eps = rnorm(n)
sbp = 0.1 * age + eps

df = data.frame(age=age, sbp=sbp)

library(ggplot2)

ggplot(data=df, aes(x = age, y = sbp)) + geom_point() + geom_smooth(method='lm')

cor(age, sbp)
cor.test(age, sbp)
```
Correlation is equivalent to a simple linear regression (linear model) that assumes the model: $sbp = a * age + intercept + err$. Fitting the model to the data is equivalent to estimate the model parameters $a$.

```{r}
lm(sbp ~ age)
```


## Simple regression: test association between two quantitative variables

Linear model:
$$
y = \beta x + cte + \varepsilon
$$
Where $\beta$ is the slope and $cte$ is called the intercept.

The simple regression is equivalent to the Pearson correlation.
Fit simple regression, aka linear model: `lm(dependant ~ independant)`

### Assumptions

1. Independence of residuals $\varepsilon$. This assumptions **must** be satisfied
2. Normality of residuals. Approximately normally distributed can be accepted.

### Model parameters and Residuals

[Source](https://drsimonj.svbtle.com/visualising-residuals).

Model the link between mpg (Miles/(US) gallon) and hp (horsepower) modeled as:
$$
\text{mpg} = \beta \text{hp} + cte + \varepsilon,
$$
where $\varepsilon$ are the residuals
```{r}
library(ggplot2)

help(mtcars)
d = mtcars

fit = lm(mpg ~ hp, data = d)
print(fit)

ggplot(data = d, aes(x = hp, y = mpg)) + geom_point() + geom_smooth(method='lm')
```
ie:
$$
\text{mpg} = \color{blue}{-0.06823}~\text{hp} + \color{blue}{30.09886} + \varepsilon
$$
```{r}
print(summary(fit))
# cor.test(d$mpg, d$hp)
```

The null hypothesis is that there is no relation between the two variables. For example, samples two random variables `var1` and `var2`:

```{r}
set.seed(1)
d$var1 = rnorm(nrow(d))
d$var2 = rnorm(nrow(d))

ggplot(data = d, aes(x = var1, y = var2)) + geom_point() + geom_smooth(method='lm')

fitnoise = lm(var2 ~ var1, data = d)
print(fitnoise)

summary(fitnoise)
# cor.test(d$var1, d$var2)
```

No significant association between the two variables (slope of 0.01158 is not significant p-value = 0.945).

Was the model valid?

Check the validity of the model, ie, normality of residuals:
```{r}
hist(fit$residuals)
```

Suspicion of non-normality of residuals, perform normality test:

```{r}
shapiro.test(fit$residuals)
```

p-value < 0.05 implying that the distribution of the data are significantly different from normal distribution.

Explore residuals:
```{r}
d$predicted = predict(fit)   # Save the predicted values
d$residuals = residuals(fit) # Save the residual values
head(d[, c("mpg", "predicted", "residuals")])

d$mpg - (d$predicted + d$residuals)

ggplot(d, aes(x = hp, y = mpg)) +
  geom_smooth(method = "lm", se = FALSE, color = "lightgrey") +  # Plot regression slope
  geom_segment(aes(xend = hp, yend = predicted), alpha = .2) +  # alpha to fade lines
  geom_point() +
  geom_point(aes(y = predicted), shape = 1) +
  theme_bw()  # Add theme for cleaner look
```

Add regressors (independent variables) to get normal residuals

```{r}
d$am = factor(d$am)
fit = lm(mpg ~ hp + am + wt, data = d)

shapiro.test(fit$residuals)
hist(fit$residuals, breaks = 10)

summary(fit)
```

### Exercise: Risk Factors Associated with Low Infant Birth Weight

```{r}
data(birthwt, package="MASS")
help(birthwt, package="MASS")
summary(birthwt)

birthwt$smoke = factor(birthwt$smoke)
summary(birthwt)
```

1. Produce two scatter plot of: (i) mother's weight by birth weight and (ii) age by birth weight.

2. Test the association of mother's weight and age with birth weight using the correlation test

3. Test the association of mother's weight and age with birth weight using linear regression. 

Conclusion ?

### Solution

```{r}
library(ggplot2)
data(birthwt, package="MASS")

ggplot(data = birthwt, aes(x = age, y = bwt)) + geom_point() + geom_smooth(method='lm')

ggplot(data = birthwt, aes(x = lwt, y = bwt)) + geom_point() + geom_smooth(method='lm')

# Correlation
cor.test(birthwt$age, birthwt$bwt)
cor.test(birthwt$lwt, birthwt$bwt)

# Linear regression
summary(lm(bwt ~ age, data=birthwt))
summary(lm(bwt ~ lwt, data=birthwt))

# Plot residuals
hist(lm(bwt ~ age, data=birthwt)$residuals)
hist(lm(bwt ~ lwt, data=birthwt)$residuals)

# Test normality of residuals
shapiro.test(lm(bwt ~ age, data=birthwt)$residuals) # OK p=0.2342
shapiro.test(lm(bwt ~ lwt, data=birthwt)$residuals) # OK p=0.5215
```
Iterate over many variables

```{r}
# obj = cor.test(birthwt$lwt, birthwt$bwt)
# names(obj)

stats = data.frame()
for(col in c('age', 'lwt')){
  obj = cor.test(birthwt[, 'bwt'], birthwt[, col])
  line = data.frame(var=col, estimate=obj$estimate, p.value=obj$p.value, stat=obj$statistic)
  stats = rbind(stats, line)
}
print(stats)
# write.csv(stats, "/tmp/stats.csv")
```

## Spearman correlation test: non-parametric test of association between two quantitative variables

```{r}
age = c(44.4, 45.9, 41.9, 53.3, 44.7, 44.1, 50.7, 45.2, 46, 47, 48, 60.1,
        57, 55, 50, 53, 45, 47, 40, 42, 43)
sbp = c(2.6,  3.1,  2.5,  5.0,  3.6,  4.0,  5.2,  2.8, 4, 4.1, 4.5,3.8, 3.5, 3, 2.2, 2.5, 1.8, 2, 1.5, 2, 2.1)
#sbp = c(2.6,  3.1,  2.5,  5.0,  3.6,  4.0,  5.2,  2.8, 4, 4.1, 4.5, -380, 3.5, 3, 2.2, 2.5, 1.8, 2, 1.5, 2, 2.1)
df = data.frame(age=age, y=sbp, age_rank=order(age), sbp_rank=order(sbp))

p = ggplot(aes(age, sbp), data=df)
p + geom_point() + geom_smooth(method='lm')
hist(lm(sbp ~ age)$residuals)

cor.test(age, sbp, method = "spearm")
cor.test(age, sbp, method = "kendall")
```

## Student t-test: compare two means

Student t-test is used to test the association between **one quantitative variable** and **one qualitative variable with 2 levels**.

### Assumptions

1. Independence of residuals. This assumptions **must** be satisfied.
2. Normality of residuals. Approximately normally distributed can be accepted.
3. Homosedasticity use T-test, Heterosedasticity use Welch t-test.


The two-sample $t$-test (Snedecor and Cochran, 1989) is used to determine if two population means are equal. There are several variations on this test.
If data are paired (e.g. 2 measures, before and after treatment for each individual) use the one-sample $t$-test of the difference. The variances of the two samples may be assumed to be equal (a.k.a. homoscedasticity) or unequal (a.k.a. heteroscedasticity).

Question: Are the mean of two samples equals ? $\bar{x} = \bar{y}$ ?

If we assume equal variance, The $t$ statistic, that is used to test whether the means are different is:

$$
t = \frac{\bar{x} - \bar{y}}{s \cdot \sqrt{\frac{1}{n_x}+\frac{1}{n_y}}},
$$

where, $\bar{x}$ and $\bar{y}$ are the sample mean of group 1 and 2

and
$$
s = \sqrt{\frac{s_{x}^2(n_x-1)+s_{y}^2(n_y-1)}{n_x+n_y-2}}
$$

### Exercise: Risk Factors Associated with Low Infant Birth Weight

1. Explore the data

2. Recode factors

3. Plot birth weight by smoking (box plot, violin plot <http://ggplot2.tidyverse.org/reference/geom_violin.html> or histogram)

4. Test the effect of smoking on birth weight

### Solutions

```{r}
data(birthwt, package="MASS")
help(birthwt, package="MASS")
summary(birthwt)

birthwt$smoke = factor(birthwt$smoke, levels=c(0, 1), labels=c("No","Yes"))

means = aggregate(bwt~smoke, data=birthwt, mean)
print(means)
stds = aggregate(bwt~smoke, data=birthwt, sd)
print(stds)

library(ggplot2)
p = ggplot(data = birthwt, aes(x = smoke, y = bwt, color=smoke, fill=smoke))

# box plot
p + geom_boxplot(alpha=0.3) + geom_jitter(height = 0, width = 0.1)

# violin plot
p + geom_violin(alpha=0.3) + geom_jitter(height = 0, width = 0.1) +
geom_point(data=means, size=5, shape=15)

# Density
p <- ggplot(data = birthwt, aes(x = bwt, color=smoke, fill=smoke))
p + geom_density(alpha=0.2) + geom_rug() + 
  geom_vline(aes(xintercept = bwt, color=smoke),
             data=means, linetype="dashed")

```
Residuals look normal

Normality test (use linear model to get the residuals)

```{r}
shapiro.test(lm(bwt ~ smoke, data=birthwt)$residuals)
```

Ok, can use a t-test

```{r}
# Assume equal variance
t.test(bwt ~ smoke, data=birthwt, var.equal=TRUE)

# Do not assume equal variance (default behavior)
t.test(bwt ~ smoke, data=birthwt)
```

## Mann-Whitney U: Nonparametric tests of two group differences 

Also knwon as: Wilcoxon Rank Sum

When to use: outliers or skewed distribution

http://data.library.virginia.edu/the-wilcoxon-rank-sum-test/


```{r}
wilcox.test(bwt ~ smoke, data=birthwt)
```

### skewed distribution

Twenty patients were randomised into two groups of ten to receive either the standard therapy A or a new treatment, B. The plasma globulin fractions after treatment was measured.

```{r}
a = c(38, 26, 29, 41, 36, 31, 32, 30, 35, 33)
b = c(45, 28, 27, 38, 40, 42, 39, 39, 40, 45)

plasma = c(a, b)
treat = factor(c(rep("A", length(a)), rep("B", length(b))))
df = data.frame(plasma=plasma, treat=treat)

# violin plot
library(ggplot2)
p = ggplot(aes(x = treat, y = plasma, color=treat, fill=treat), data=df)
p + geom_violin(alpha=0.3) + geom_jitter(height = 0, width = 0.1)

# Basic test
wilcox.test(plasma ~ treat, data=df, exact=FALSE, correct=FALSE)

# Improved p-value computation
wilcox.test(plasma ~ treat, data=df)

t.test(plasma ~ treat, data=df)
```

## Anova: two groups or more (one factor with two levels or more)

Anova: Analysis of variance

The Anova measures the association between one quantitative variable and one qualitative (possibly with more than 2 levels).

### Assumptions

1. The samples are randomly selected in an independent
manner from the k treatment populations.
2. All k populations have distributions that are approximately normal. Check by plotting groups distribution.
3. The k population variances are equal. Check by plotting groups distribution.

Sources:

- [F-test assumption violation](https://www.quality-control-plan.com/StatGuide/ftest_ass_viol.htm)
- [Anova assumptions](https://sites.ualberta.ca/~lkgray/uploads/7/3/6/2/7362679/slides_-_anova_assumptions.pdf)

Anova in R:

- `summary(aov(lm(variable ~ group, data=xxx)))`, ie: summary of anova of linear model or `summary(aov(variable ~ group, data=xxx))`
- or `Anova(lm(variable ~ group, data=xxx), type="II")` from `car` package.

```{r}
data(iris)
help(iris)
summary(iris)

means = aggregate(Petal.Width ~ Species, data=iris, mean)
aggregate(Petal.Width ~ Species, data=iris, sd)

library(ggplot2)

p <- ggplot(data = iris, aes(x = Species, y=Petal.Width, color=Species, fill=Species))
p + geom_jitter() + geom_point(data=means, size=5, shape=15)


p + geom_violin(alpha=0.3)

# Density
p <- ggplot(data = iris, aes(x = Petal.Width, color=Species, fill=Species))
p + geom_density(alpha=0.2) + geom_rug() + geom_vline(aes(xintercept = Petal.Width, color=Species),data=means, linetype="dashed")

summary(aov(Petal.Width ~ Species, data=iris))
```

Alternatives and detail of internals coding of factors

```{r}
nrow(iris)
# Print design matrix
design = model.matrix(Petal.Width ~ Species, data=iris)
head(model.matrix(Petal.Width ~ Species, data=iris))
model = lm(Petal.Width ~ Species, data=iris)
summary(aov(model))

library(car)
Anova(model, type="II")                    # Can use type="III"
```

Check the residuals' normality assumption:
```{r}
hist(residuals(model))
shapiro.test(residuals(model))
```

Residuals look normal, but shapiro test reject normality assumption: some outliers? Inspect deviation from normal distribution using QQ plot of residual vs theoretical distribution.
```{r}
plot(model, 2)
```

## Two-way Anova: test association between one quantitative variable and two factors

Anova measure the association between one quantitative variable and two qualitative (more than 2 levels) factors

[source: www.sheffield.ac.uk](https://www.sheffield.ac.uk/polopoly_fs/1.536444%21/file/MASH_2way_ANOVA_in_R.pdf)

Common Applications: Comparing means for combinations of two independent categorical variables (factors).

Data: The data set Diet.csv contains information on 78 people who undertook one of three diets. There is background information such as age, gender (Female=0, Male=1) and height. The aim of the study was to see which diet was best for losing weight but it was also thought that the best diets for males and females may be different so the independent
variables are diet and gender. 

```{r}
df = read.csv("data/diet.csv")
summary(df)
df$Diet = factor(df$Diet)
df$gender = factor(df$gender)
df$weightlost = df$pre.weight - df$weight6weeks
df[, 'weightlost'] = df[, 'pre.weight'] - df[, 'weight6weeks']
```

Age effect on weight loss

```{r}
summary(lm(weightlost ~ Age, data=df))

cor.test(df$weightlost, df$Age)
```

One-way Anova: test diet on weight loss
```{r}
# Visualization
means = aggregate(weightlost ~ Diet, data=df, mean)
p <- ggplot(data = df, aes(x = Diet, y=weightlost, fill=Diet, color=Diet))
p + geom_jitter() + geom_point(data=means, size=5, shape=15)
p + geom_violin(alpha=0.3) + geom_jitter(height = 0, width = 0.1)

# ANOVA
model = aov(weightlost ~ Diet, data=df)
summary(model)

# Check Normality
hist(model$residuals)
shapiro.test(model$residuals)
```

Two-way Anova, simple additive model
```{r}
model = aov(weightlost ~ Diet + gender, data=df)
summary(model)
hist(model$residuals)
shapiro.test(model$residuals)
```

Two-way Anova, full model with interaction
```{r}
model = aov(weightlost ~ Diet + gender + Diet:gender, data=df)
summary(model)
hist(model$residuals)
```


## Muliple regression: test association between one quantitative variable and many quantitative variables

### Linear model

Given $n$ random samples $(y_i, x_{i}^1, \ldots, x_{i}^p), \, i = 1, \ldots, n$, the linear regression models the relation between the observations $y_i$ and the independent variables $x_i^p$ is formulated as

$$
y_i = \beta_0 + \beta_1 x_{i}^1 + \cdots + \beta_p x_{i}^p + \varepsilon_i \qquad i = 1, \ldots, n
$$

### Terminology

- **An independent variable (IV)**. It is a variable that stands alone and isn't changed by the other variables you are trying to measure. For example, someone's age might be an independent variable. Other factors (such as what they eat, how much they go to school, how much television they watch) aren't going to change a person's age. In fact, when you are looking for some kind of relationship between variables you are trying to see if the independent variable causes some kind of change in the other variables, or dependent variables. In Machine Learning, these variables are also called the **predictors**.

- A **dependent variable**. It is something that depends on other factors. For example, a test score could be a dependent variable because it could change depending on several factors such as how much you studied, how much sleep you got the night before you took the test, or even how hungry you were when you took it. Usually when you are looking for a relationship between two things you are trying to find out what makes the dependent variable change the way it does. In Machine Learning this variable is called a **target variable**.


```{r}
# simulated data
set.seed(5)
experience = c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
education = c(0, 3, 5, 0, 3, 5, 0, 3, 5, 0, 3, 5)
eps = 100 * rnorm(length(experience))
dummy = rnorm(length(experience))
salary = 1000 + 110 * experience + 150 * education + eps

# Design matrix
model.matrix(salary ~ experience + education + dummy)

# Linear model
mod = lm(salary ~ experience + education + dummy)

# hist(mod$residuals)
summary(mod)
```

Fitted model:

$$
salary = 1074.69 + 97.37 * experience + 153.97 * education + 60.70 * dummy
$$

The problem with correlated variables

```
experience_month = 12 * experience + rnorm(length(experience))
summary(lm(salary ~ experience_month + experience + education))
```

## Ancova: test association between one quantitative variable and mix of quantitative variables and factors

Ancova: Analysis of Covariance

```{r}
# simulated data
experience = c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
education = c(0, 3, 5, 0, 3, 5, 0, 3, 5, 0, 3, 5)
education_fac = factor(education, labels = c("bachelor", "licence", "master"))

eps = 100 * rnorm(length(experience))
salary = 1000 + 110 * experience + 150 * education + eps
df = data.frame(salary, experience, education_fac)

mod = lm(salary ~ experience + education_fac, data=df)
summary(aov(mod))

model.matrix(salary ~ experience + education_fac)

```

```{r}
salary_pred = predict(mod, df)
ggplot(aes(x=experience, y=salary, color = education_fac), data=df) + geom_point() + 
geom_line(aes(y= salary_pred))
```

### ANCOVA: models with interaction

```{r}
set.seed(1)
# simulated data
experience = c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
education = c(0, 3, 5, 0, 3, 5, 0, 3, 5, 0, 3, 5)
education_fac = factor(education, labels = c("bachelor", "licence", "master"))
n = length(experience)

eps = 200 * rnorm(n)
salary = rep(0, n)

salary[education_fac=="bachelor"] = 1000 + 100 * experience[education_fac=="bachelor"]
salary[education_fac=="licence"] = 1500 + 200 * experience[education_fac=="licence"]
salary[education_fac=="master"] = 2000 + 400 * experience[education_fac=="master"]

salary = salary + eps

df = data.frame(salary, experience, education_fac)

mod = lm(salary ~ education_fac + experience : education_fac, data=df)
summary(aov(mod))

g = ggplot(aes(x=experience, y=salary, color=education_fac), data=df)
g + geom_point() + geom_smooth(method=lm)
```

Interaction R formula notation:

- Additive model `a + b`
- Full model: `a * b = a + b + a:b`


```{r}
# summary(aov(salary ~ education_fac + experience + experience:education_fac, data=df))

summary(aov(salary ~ education_fac + education_fac : experience, data=df))

```
## Chi-square test: association between two factors

Example: 15 samples, 10 first with exposition, 5 last without. 8 first with disease, 6 without, the last with. $\chi^2$ tests the association between exposition and disease.

Idea: Computes the chi-square, $\chi^2$, statistic and $p$-value for the hypothesis test of independence of frequencies in the observed contingency table (cross-table). The observed frequencies are tested against an expected contingency table obtained by computing expected frequencies based on the marginal sums under the assumption of independence.

```{r}
# 10 first with exposition, last 5 without
exposition = c(rep(1, 10), rep(0, 5))
# 8 first with disease, 6 without, the last with.
disease = c(rep(1, 8), rep(0, 6),  1)

print("Observed contingency table:")
tbl = table(exposition, disease)
tbl

chisq.test(tbl)
```

## Two samples proportion Z-test (or Chi-square test): difference of two proportions

Test the difference between two proportions:

$$
z = \frac{p_1 - p_2}{\sigma}
$$
with 
$$
\sigma = \sqrt{p(1-p)(1/n_1+1/n_2)}
$$
and 
$$
p=\frac{n_1 p_1+n_2 p_2}{n_1 + n_2}
$$

There is more CHD in males group than in females group.

```{r}

datafile = "data/Framingham.csv"
fhs <- read.csv(datafile)

# Recode factor
fhs$sex = factor(fhs$sex, labels=c("M", "F"))
fhs$chdfate = factor(fhs$chdfate, labels=c("No", "Yes"))
summary(fhs)

chdfate.f = fhs$chdfate[fhs$sex == "F"]
n.f = length(chdfate.f)

chdfate.m = fhs$chdfate[fhs$sex == "M"]
n.m = length(chdfate.m)

nchd.f = summary(chdfate.f)[2]
nchd.m = summary(chdfate.m)[2]
cat("Female with CHD:", nchd.f, "over", n.f)
cat("Male with CHD:", nchd.m, "over", n.m)

prop.test(c(nchd.f, nchd.m), c(n.f, n.m))
```

## Risks of multiple testing

### Exercise: Simulated random data

Generate two sets of 50 samples normally distributed: $x \sim \mathcal{N}(\mu=0, \sigma=1)$ and $x \sim \mathcal{N}(\mu=0, \sigma=1)$.
Compute, the correlations, and its p-value.
Repeate this experiment 1000 times.

1. Plot the distribution of the correlations.

2. Plot the distribution of the p-values.

3. Compute the proportion of false positive, using a threshold $0.05$ on p-value. 

### Solution

```{r}
n = 50

nrepeat = 1000
corrs = rep(0, nrepeat)
pvals = rep(0, nrepeat)

for(i in 1:nrepeat){
  x = rnorm(n)
  y = rnorm(n)
  obj = cor.test(x, y)
  # explore using names(obj)
  corrs[i] = obj$estimate
  pvals[i] = obj$p.value
}
```

Distribution of the correlation

```{r}
library(ggplot2)
df = data.frame(corrs=corrs, pvals=pvals)
p = ggplot(aes(x=corrs), data=df)

#p + geom_density()
p + geom_histogram()
```

Distribution of the p-values

```{r}
p = ggplot(aes(x=pvals), data=df)
p + geom_histogram(bins=30) + geom_vline(xintercept=0.05)

cat("Proportion of false positives:", sum(pvals < 0.05) / length(pvals)) 
```

```{r}

sbp_bl =  c(1 ,2, 3, 2, 1, 4, 3, 1 , 5, 3)
cond = c(rep("Y", 5), rep("N", 5))
sbp_fu = rep(0, 10)
sbp_fu[cond == "Y"]  = sbp_bl[cond == "Y"] - 1  + rnorm(5)
sbp_fu[cond == "N"]  = sbp_bl[cond == "N"] + rnorm(5)

df = data.frame(sbp_bl, sbp_fu, cond)

df$diff = df$sbp_fu - df$sbp_bl


t.test(diff ~ cond, data=df, var.equal = TRUE)

summary(aov(lm(diff ~ cond, data=df)))
```
